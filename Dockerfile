FROM nginx:1.27.0

COPY /index.html /usr/share/nginx/html/

COPY /test.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

