## Запуск контейнера nginx

Установите Docker, если он не установлен (ссылка на документацию по установке [Link text](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository))

Используя Dockerfile, сделайте сборку образа с тегом:
```
docker build -t custom-nginx-image .
```

Запустите контейнер:
```
docker run -d -p 83:80 --name test-nginx custom-nginx-image
```

Проверьте запущен ли контейнер:
```
docker ps
```

