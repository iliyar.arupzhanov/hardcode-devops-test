# Hardcode DevOps test
## Установка веб-сервера и настройка Nginx

Обновите список пакетов:

```
   sudo apt update
```

Установите Nginx из репозитория Ubuntu:

```
   sudo apt install nginx
```

Проверьте установку
```
   sudo nginx -v
```

Создайте простой HTML-файл index.html с текстом "Hello, DevOps World!", или воспользуйтесь файлом из этого репозитория поместите его в /var/www/html

Поместите Nginx конфигурацию `test.conf` в `/etc/nginx/sites-available/` , предварительно заменив имя сервера на ваш ip address или на localhost, если запускаете у себя локально:
```
   server_name localhost your-ip-address
```

Удалите `default.conf` в `/etcsites-enabled` и сделайте symlink:
```
sudo ln -s /etc/nginx/sites-available/test.conf /etc/nginx/sites-enabled/

```
Проверьте правильность конфигурации nginx на ошибки перед рестартом:
```
   sudo nginx -t
```

Перезапустите nginx для применения изменений
```
   sudo systemctl restart nginx
```

Откройте браузер и перейдите по адресу:
```
   http://localhost или http://your-ip-address
```
