# Hardcode DevOps test

## Установка и настройка web server Nginx
Для установки и настройки Nginx проверьте INSTRUCTIONS_NGINX.md

## Запуск контейнера Nginx
Для сборки и запуска Nginx контейнера проверьте INSTRUCTIONS_DOCKER.md
